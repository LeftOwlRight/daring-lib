_G.DaringLib = _G.DaringLib or {}

-------------------- Player Info --------------------

-- Get your game name
-- @return string
function DaringLib:get_my_game_name()
    return NetworkAccountSTEAM:username_id()
end

-- Get the steam name
-- @return string
function DaringLib:get_my_steam_name()
    return Steam:username()
end

-- Get local player table
-- @return table
function DaringLib:get_local_player()
    return managers.player:local_player()
end

-- Get table for local player unit
-- @return table
function DaringLib:get_local_player_unit()
    return managers.player:player_unit()
end

-------------------- Player Unit Info --------------------

-- Get position for local player unit
-- @param tunit_name : string | nil
-- @param tunit_name : "player" | "head"
-- @return userdata
function DaringLib:get_local_player_position(unit_name)
    local player_unit = managers.player:player_unit()
	
	if not unit_name or unit_name == "player" or string.upper(unit_name) == "PLAYER" then
		return player_unit:position()
	elseif unit_name == "head" or string.upper(unit_name) == "HEAD" then
		return player_unit:movement():m_head_pos()
	end
end

-- Get rotation for local player unit
-- @return userdata
function DaringLib:get_local_player_rotation()
    return managers.player:player_unit():rotation()
end

-------------------- Player Weapon --------------------

-- Get weapon data table by weapon id
-- @param tweapon_id : string
-- @return table
function DaringLib:get_local_player_weapon_data(weapon_id)
	local weapon_data = tweak_data.weapon[weapon_id]
	
	if weapon_data then
		return weapon_data
	end
end

-- Get the ID of the currently held weapon
-- @return string
function DaringLib:get_local_player_weapon_id()
	local player_unit = managers.player:player_unit()
	local weapon = player_unit:inventory():equipped_unit()
	local weapon_id = weapon:base():get_name_id()
	
	return weapon_id
end

-- Get the type of the currently held weapon
-- @return string
function DaringLib:get_local_player_weapon_type()
	local player_unit = managers.player:player_unit()
	local weapon = player_unit:inventory():equipped_unit()
	local weapon_type = weapon:base():weapon_tweak_data().categories[1]
	
	return weapon_type
end