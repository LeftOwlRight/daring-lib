_G.DaringLib = _G.DaringLib or {}

-- Get Network Peers
-- @return table
function DaringLib:get_peers()
	self._peers = {}

	if managers.network and managers.network.session and managers.network:session() then
		for peer_id = 1, 4 do
			 if peer_id ~= managers.network:session():local_peer():id() then
				local peer = managers.network:session():peer(peer_id)
				
				if peer then
					self._peers[#self._peers+1] = peer
				end
			end
		end
		
		return self._peers
	end
end

-- Get network peers and local peer
-- @return table
function DaringLib:get_peers_and_local_peer()
	local peers = DaringLib:get_peers()
	local local_peer_id = managers.network:session():local_peer():id()
	local local_peer = managers.network:session():peer(local_peer_id)
	table.insert(peers, local_peer_id, local_peer)
	
	return peers
end

-- Get peer table
-- @param peer_id : number (Peer ID)
-- @return table
function DaringLib:get_peer_by_id(peer_id)
	local peer = managers.network:session():peer(peer_id)
	
	if peer then
		return peer
	end
end

-- Get Peer Level
-- @param peer_id : number (Peer ID)
-- @return number
function DaringLib:get_peer_level(peer_id)
	local peer = managers.network:session():peer(peer_id)
	
	if peer then
		return peer:level()
	end
end

-- Get Peer Rank
-- @param peer_id : number (Peer ID)
-- @return number
function DaringLib:get_peer_rank(peer_id)
	local peer = managers.network:session():peer(peer_id)
	
	if peer then
		return peer:rank()
	end
end

-- Get Peer Platform
-- @param peer_id : number (Peer ID)
-- @return string("STEAM" | "EPIC")
function DaringLib:get_peer_platform(peer_id)
	local peer = managers.network:session():peer(peer_id)
	
	if peer then
		return peer:account_type_str()
	end
end

-- Get Peer Mods
-- @param peer_id : number (Peer ID)
-- @return table
function DaringLib:get_peer_mods(peer_id)
	local peer = managers.network:session():peer(peer_id)
	
	if peer then
		return peer:synced_mods()
	end
end

-- Get Peer Latency(Ping)
-- @param peer_id : number (Peer ID)
-- @return number
function DaringLib:get_peer_ping(peer_id)
	local peer = managers.network:session():peer(peer_id)
	
	if peer then
		return peer:qos().ping
	end
end

-- Get position for peer unit
-- peer_id : number (Peer ID)
-- @param unit_name : string | nil
-- @param unit_name : "player" | "head"
-- @return userdata
function DaringLib:get_peer_position(peer_id, unit_name)
	local peer = managers.network:session():peer(peer_id)
	
	if peer then
		local peer_unit = peer:unit()
		
		if not unit_name or unit_name == "player" or string.upper(unit_name) == "PLAYER" then
			return peer_unit:position()
		elseif unit_name == "head" or string.upper(unit_name) == "HEAD" then
			return peer_unit:movement():m_head_pos()
		end
	end
end

-- Get rotation for peer unit
-- @return userdata
function DaringLib:get_peer_rotation(peer_id)
	local peer = managers.network:session():peer(peer_id)
	
	if peer then
		return peer:unit():rotation()
	end
end