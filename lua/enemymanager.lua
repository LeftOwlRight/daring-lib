_G.DaringLib = _G.DaringLib or {}

-- Return the current amount of enemies on the field
-- @return number
function DaringLib:get_enemies_amount()
	return managers.enemy._enemy_data.nr_units
end

-- Return wether the unit is a enemy
-- @return boolean
function DaringLib:is_enemy(unit)
	if not alive(unit) then
		return
	end
	
	return managers.enemy:is_enemy(unit)
end

-- Return wether the unit is a civilian
-- @return boolean
function DaringLib:is_civilian(unit)
	if not alive(unit) then
		return
	end
	
	return managers.enemy:is_civilian(unit)
end

-- Return enemy type("taser" "tank" ...)
-- @return string
function DaringLib:get_enemy_type(unit)
	if not alive(unit) then
		return
	end

	return unit:base()._tweak_table
end

-- Return Weapon ID used by the enemy
-- @return string
function DaringLib:get_enemy_weapon_id(unit)
	if not alive(unit) then
		return
	end
	
	local weapon_name = ""
	local weapon_data = unit:inventory():equipped_unit()
	if weapon_data then
		weapon_name = string.gsub(tostring(weapon_data:base()._name_id), "_npc", "", 1)
	end
	
	return weapon_name
end

-- Return the distance between the enemy and you
-- @param ene_unit : userdata(unit)
-- @param to_unit : userdata(unit)
-- @return number
function DaringLib:get_enemy_distance(ene_unit, to_unit)
	if not alive(ene_unit) then
		return
	end
	
	if to_unit then
		local distance = mvector3.direction(Vector3(), ene_unit:movement():m_com(), to_unit:position())
		return distance
	end
end

-- Return enemy health
-- @return number
function DaringLib:get_enemy_health(unit)
	if not alive(unit) then
		return
	end
	
	local health = unit:character_damage()._health or 0
	return health
end

-- Check enemy can be surrender
-- @return boolean
function DaringLib:chk_surrender(unit)
	if not alive(unit) then
		return
	end

	local unit_surrender = tweak_data.character[unit:base()._tweak_table].surrender

	if not unit_surrender or unit_surrender == tweak_data.character.presets.surrender.special then
		return false
	else
		return true
	end
end

-- Return units with a distance less than 'distance' from the position 'who_pos'
-- @param who_pos : userdata (if 'nil', auto pass in "player_unit")
-- @param distance : number (if 'nil', auto pass in 800)
-- @return a table(enemies)
function DaringLib:get_nearby_enemies(who_pos, distance)
	if not managers.player:player_unit() then
		return
	end
	
	local who_pos = who_pos or managers.player:player_unit():position()
	local distance = distance or 800
	local enemies = World:find_units_quick("sphere", who_pos, distance, managers.slot:get_mask("trip_mine_targets"))
	
	if enemies then
		return enemies
	end
end
